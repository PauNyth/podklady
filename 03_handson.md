# Git hands on (3rd lecture 2023-03-20)

```bash
# Install GIT on RedHat/CentOS/Rocky
yum install git

# Install GIT on Ubuntu/Debian
apt install git

# Export environment variables in ~/.bashrc
export GIT_AUTHOR_NAME="Name Surname"
export GIT_AUTHOR_EMAIL=name@domain.tld
export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"

```

1. Clone the repository for Ruby on Rails framework at https://github.com/rails/rails
   1. Explore the version history by visualizing it as a graph.
   1. Who was the last person to modify README.md? (Hint: use git log with an argument)
   1. What is the oldest commit message and year? What tool was used before Git in this project?
   1. What was the commit message associated with the last modification to the Dalli GEM, line of Gemfile? (Hint: use git blame and git show)
   1. Modify README.md. What happens when you do `git stash`? Run `git stash list`. Inspect the repository. Run `git stash pop` to undo what you did with git stash. In what scenario might this be useful?
   1. Git provides a configuration file (or dotfile) called `~/.gitconfig`. Create an alias in `~/.gitconfig` so that when you run `git graph`, you get the output of `git log --all --graph --decorate --oneline`. Information about git aliases can be found [here](https://git-scm.com/docs/git-config#Documentation/git-config.txt-alias).
   1. BASH provides a configuration file called `~/.bashrc`. Create aliases in `~/.bashrc` so that when you run `ggl`, you get the output of `git log --all --graph --decorate --oneline` and when you run `ggh`, you get the output of `git graph`.
